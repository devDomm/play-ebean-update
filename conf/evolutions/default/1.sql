# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table company (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  constraint pk_company primary key (id)
);

create table computer (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  introduced                    timestamp,
  discontinued                  timestamp,
  company_id                    bigint,
  constraint pk_computer primary key (id)
);

create table user (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  comment                       varchar(255),
  constraint pk_user primary key (id)
);

create table user_computer_relation (
  id                            bigint auto_increment not null,
  user_id                       bigint,
  computer_id                   bigint,
  is_private                    boolean,
  constraint pk_user_computer_relation primary key (id)
);

create table user_history (
  id                            bigint auto_increment not null,
  type                          varchar(255),
  field                         varchar(255),
  oldvalue                      varchar(255),
  newvalue                      varchar(255),
  history_header_id             bigint,
  constraint pk_user_history primary key (id)
);

create table user_history_header (
  id                            bigint auto_increment not null,
  user_id                       bigint,
  constraint pk_user_history_header primary key (id)
);

alter table computer add constraint fk_computer_company_id foreign key (company_id) references company (id) on delete restrict on update restrict;
create index ix_computer_company_id on computer (company_id);

alter table user_computer_relation add constraint fk_user_computer_relation_user_id foreign key (user_id) references user (id) on delete restrict on update restrict;
create index ix_user_computer_relation_user_id on user_computer_relation (user_id);

alter table user_computer_relation add constraint fk_user_computer_relation_computer_id foreign key (computer_id) references computer (id) on delete restrict on update restrict;
create index ix_user_computer_relation_computer_id on user_computer_relation (computer_id);

alter table user_history add constraint fk_user_history_history_header_id foreign key (history_header_id) references user_history_header (id) on delete restrict on update restrict;
create index ix_user_history_history_header_id on user_history (history_header_id);

alter table user_history_header add constraint fk_user_history_header_user_id foreign key (user_id) references user (id) on delete restrict on update restrict;
create index ix_user_history_header_user_id on user_history_header (user_id);


# --- !Downs

alter table computer drop constraint if exists fk_computer_company_id;
drop index if exists ix_computer_company_id;

alter table user_computer_relation drop constraint if exists fk_user_computer_relation_user_id;
drop index if exists ix_user_computer_relation_user_id;

alter table user_computer_relation drop constraint if exists fk_user_computer_relation_computer_id;
drop index if exists ix_user_computer_relation_computer_id;

alter table user_history drop constraint if exists fk_user_history_history_header_id;
drop index if exists ix_user_history_history_header_id;

alter table user_history_header drop constraint if exists fk_user_history_header_user_id;
drop index if exists ix_user_history_header_user_id;

drop table if exists company;

drop table if exists computer;

drop table if exists user;

drop table if exists user_computer_relation;

drop table if exists user_history;

drop table if exists user_history_header;

