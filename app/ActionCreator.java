import play.Logger;
import play.http.DefaultActionCreator;
import play.mvc.Action;
import play.mvc.Http.Request;

import javax.inject.Singleton;
import java.lang.reflect.Method;

@Singleton
public class ActionCreator extends DefaultActionCreator {

    @Override
    public Action createAction(Request request, Method actionMethod) {
        if (Logger.isDebugEnabled()) {
            Logger.debug("onRequest [Request: " + request.method() + " - " + request.path() + ", Method: " + actionMethod.getName() + "]");
        }
        return super.createAction(request, actionMethod);
    }

}
