package models;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.Cache;

import javax.persistence.*;
import java.util.List;

@Entity
@Cache(readOnly = true)
public class UserHistoryHeader extends Model {

    @Id
    public Long id;

    @ManyToOne
    public User user;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "historyHeader")
    public List<UserHistory> historyEntries;

    public static final com.avaje.ebean.Model.Find<Long, UserHistoryHeader> find = new Find<Long, UserHistoryHeader>(){};
}
