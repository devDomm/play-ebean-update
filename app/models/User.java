package models;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.Cache;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
@Cache
public class User extends Model {

    @Id
    public Long id;

    public String name;

    public String comment;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    public List<UserComputerRelation> relations;

    public static Find<Long,User> find = new Find<Long,User>(){};

    public List<Computer> getAssignedComputers() {
        List<Computer> computerList = new ArrayList<>();
        if(this.relations != null) {
            for (UserComputerRelation relation : this.relations) {
                computerList.add(relation.computer);
            }
        }
        return computerList;
    }
}
