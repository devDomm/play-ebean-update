package models;

import com.avaje.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class UserComputerRelation extends Model {

    @Id
    public Long id;

    @ManyToOne
    @Column(nullable = false)
    public User user;

    @ManyToOne
    @Column(nullable = false)
    public Computer computer;

    public boolean isPrivate;

    public static Find<Long,UserComputerRelation> find = new Find<Long,UserComputerRelation>(){};
}
