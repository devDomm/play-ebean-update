package models;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.Cache;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
@Cache(readOnly = true)
public class UserHistory extends Model {

    @Id
    public Long id;

    public String type;

    public String field;

    public String oldvalue;

    public String newvalue;

    @ManyToOne
    public UserHistoryHeader historyHeader;

    public UserHistory(String type, String field, String oldvalue, String newvalue) {
        this.type = type;
        this.field = field;
        this.oldvalue = oldvalue;
        this.newvalue = newvalue;
    }

    public static final com.avaje.ebean.Model.Find<Long, UserHistory> find = new Find<Long, UserHistory>(){};
}
