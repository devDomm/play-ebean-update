package models;

import java.util.*;
import javax.persistence.*;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.Cache;
import play.data.format.*;
import play.data.validation.*;

import com.avaje.ebean.*;

/**
 * Computer entity managed by Ebean
 */
@Entity
@Cache(enableQueryCache = true)
public class Computer extends Model {

	@Id
    public Long id;
    
    @Constraints.Required
    public String name;
    
    @Formats.DateTime(pattern="yyyy-MM-dd")
    public Date introduced;
    
    @Formats.DateTime(pattern="yyyy-MM-dd")
    public Date discontinued;
    
    @ManyToOne
    public Company company;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "computer")
    public List<UserComputerRelation> relations;

    /**
     * Generic query helper for entity Computer with id Long
     */
    public static Find<Long,Computer> find = new Find<Long,Computer>(){};
    
    /**
     * Return a paged list of computer
     *
     * @param page Page to display
     * @param pageSize Number of computers per page
     * @param sortBy Computer property used for sorting
     * @param order Sort order (either or asc or desc)
     * @param filter Filter applied on the name column
     */
    public static PagedList<Computer> page(int page, int pageSize, String sortBy, String order, String filter) {
        return null;
    }
    
}

