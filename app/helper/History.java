package helper;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.ValuePair;
import com.avaje.ebean.event.BeanPersistAdapter;
import com.avaje.ebean.event.BeanPersistRequest;
import com.avaje.ebeaninternal.server.core.PersistRequest;
import com.avaje.ebeaninternal.server.core.PersistRequestBean;
import models.User;
import models.UserHistory;
import models.UserHistoryHeader;

import java.util.*;

public class History extends BeanPersistAdapter {
    @Override
    public boolean isRegisterFor(Class<?> cls) {
        return cls == User.class;
    }

    @Override
    public boolean preUpdate(BeanPersistRequest<?> request) {

        //noinspection unchecked
        User newBean = (User) request.getBean();
        User oldBean = getOldBeanById(newBean.id);

        List<UserHistory> history = createHistory(request, oldBean, newBean);

        if (history != null && !history.isEmpty()) {
            saveHistory(newBean, history);
        }

        return true;
    }

    private User getOldBeanById(Long id) {
        User user = Ebean.find(User.class).setUseCache(false).setId(id).fetch("relations").findUnique();

        return user;
    }

    private List<UserHistory> createHistory(BeanPersistRequest<?> request, User oldBean, User newBean) {

        List<UserHistory> history = new ArrayList<>();

        boolean stateless = PersistRequest.Type.UPDATE.equals(((PersistRequestBean) request).getType()) && ((PersistRequestBean) request).getEntityBeanIntercept().isNew();
        for (Map.Entry<String, ValuePair> entry : getChanges(oldBean, newBean, stateless).entrySet())  {
            UserHistory historyEntry = new UserHistory("C", entry.getKey(), getValue(entry.getValue().getOldValue()), getValue(entry.getValue().getNewValue()));

            newBean.getAssignedComputers();
            oldBean.getAssignedComputers();

            history.add(historyEntry);
        }
        return history;
    }

    private Map<String, ValuePair> getChanges(User oldBean, User newBean, boolean ignoreNullValues) {
        Map<String, ValuePair> changes = new HashMap<>();

        for (Map.Entry<String, ValuePair> entry : Ebean.diff(newBean, oldBean).entrySet()) {
                if (!ignoreNullValues
                        || (entry.getValue().getNewValue() != null && !entry.getValue().getNewValue().equals(""))
                        || (entry.getValue().getOldValue() != null && !entry.getValue().getOldValue().equals("")
                        && entry.getValue().getNewValue() != null && entry.getValue().getNewValue().equals(""))) {

                    String[] props = entry.getKey().split("\\.");

                    // This requires the rootClass to have a property called id which is the @Id of the entity bean.
                    // This is a poor convention, but a workaround for https://github.com/ebean-orm/avaje-ebeanorm/issues/406
                    if (props.length > 1 && props[1].toLowerCase().equals("id")) {
                        ValuePair valuePair = new ValuePair();

                        if (entry.getValue().getOldValue() != null) {
                            valuePair.setOldValue(getBeanById(oldBean.getClass(), (Long) entry.getValue().getOldValue()));
                        }
                        if (entry.getValue().getNewValue() != null) {
                            valuePair.setNewValue(getBeanById(oldBean.getClass(), (Long) entry.getValue().getNewValue()));
                        }
                        changes.put(props[0], valuePair);
                    } else {
                        changes.put(entry.getKey(), entry.getValue());
                    }
                }
        }

        return changes;
    }

    private <C> C getBeanById(Class<C> clazz, Long id) {
        return Ebean.find(clazz, id);
    }

    private String getValue(Object value) {
        return value != null ? value.toString() : null;
    }

    public void saveHistory(User entity, List<UserHistory> history) {
        UserHistoryHeader header = new UserHistoryHeader();
        header.user = entity;
        header.historyEntries = history;

        header.save();
    }
}
