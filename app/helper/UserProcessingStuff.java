package helper;

import models.User;

public class UserProcessingStuff {

    public static void funnyStaticMethod(User user) {

        if(user.name == null) {
            user.name = "something";
        } else {
            user.name = user.name + "1";
        }

        user.update();
    }
}
